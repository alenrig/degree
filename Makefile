COMPILER=latexmk
SRC=degree.tex
FLAGS=-f -use-make -silent -shell-escape -interaction=nonstopmode -synctex=3 -file-line-error -pdf
ARTIFACTS=*.aus *.aux *.bbl *.blg *.eps *.fdb_latexmk *.fls *.gnuplot *-gnuplottex-* *.gnuploterrors *.log *.pdf *.toc *.synctex.gz *.synctex

all:
	$(COMPILER) $(FLAGS) $(SRC)

view:
	$(COMPILER) -pvc $(FLAGS) $(SRC)

clean:
	rm -f $(ARTIFACTS)
