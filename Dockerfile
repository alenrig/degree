FROM ubuntu:18.04

ENV DEBIAN_FRONTEND="noninteractive"

RUN apt update && \
    apt install -y \
    make \
    gnuplot \
    language-pack-ru

ENV LANGUAGE=ru_RU.UTF-8 \
    LANG=ru_RU.UTF-8 \
    LC_ALL=ru_RU.UTF-8

RUN locale-gen ru_RU.UTF-8 && dpkg-reconfigure locales

RUN apt install -y \
    latexmk \
    texlive \
    texlive-xetex \
    texlive-pictures \
    texlive-lang-cyrillic \
    texlive-font-utils \
    texlive-science
